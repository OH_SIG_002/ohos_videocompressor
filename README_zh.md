# ohos_videocompressor

## 介绍
videoCompressor是一款ohos高性能视频压缩器。

目前实现的能力：
- 支持视频压缩

## 使用本工程
有两种方式可以下载本工程：
1. 开发者如果想要使用本工程,可以使用git命令
```
git clone https://gitee.com/openharmony-sig/ohos_videocompressor.git --recurse-submodules
```
2. 点击下载按钮，把本工程下到本地，再把[third_party_bounds_checking_function](https://gitee.com/openharmony/third_party_bounds_checking_function)代码下载后，放入videoCompressor/src/cpp/boundscheck目录下，这样才可以编译通过。


## 下载安装

```
ohpm install @ohos/videocompressor
```

OpenHarmony ohpm
环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)

## X86模拟器配置

使用模拟器运行应用/服务
## 使用

### 1、视频压缩接口展示

```
let videoCompressor = new VideoCompressor();
videoCompressor.compressVideo(getContext(),this.selectFilePath,CompressQuality.COMPRESS_QUALITY_HIGH).then((data) => {
    if (data.code == CompressorResponseCode.SUCCESS) {
        console.log("videoCompressor HIGH message:" + data.message + "--outputPath:" + data.outputPath);
    } else {
        console.log("videoCompressor HIGH code:" + data.code + "--error message:" + data.message);
    }
    }).catch((err) => {
        console.log("videoCompressor HIGH get error message" + err.message);
    })
```

## 支持的视频规格
支持的解封装格式：

| 媒体格式 | 封装格式     |
| -------- | ------------ |
| 视频     | mp4、mpeg.ts |

支持的视频解码格式：

| 视频解码类型                  |
|-------------------------|
| AVC(H.264)、 HEVC(H.265) |

支持的音频解码格式：

| 音频解码类型   |
| -------------- |
| AAC |

支持的视频编码格式：

| 视频编码类型            |
| ----------------------- |
| AVC(H.264)、 HEVC(H.265) |

支持的音频编码格式：

| 音频编码类型 |
| ------------ |
| AAC          |

支持的封装格式：

| 媒体格式 | 封装格式     |
| -------- | ------------ |
| 视频     | mp4|

## 接口说明

| 方法名                                                       | 入参                                                         | 接口描述     |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------ |
| compressVideo(context: Context, inputFilePath: string, quality: CompressQuality): Promise<CompressorResponse> | context:上下文，inputFilePath: 需要压缩的视频路径, quality: 压缩视频质量 | 视频压缩接口 |

单元测试用例详情见[TEST.md](./TEST.md)
## 约束与限制

在下述版本验证通过：

DevEco Studio: (5.0.3.500), SDK: API12 (5.0.0.25)
DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release (5.0.0.66)

## 目录结构

```
|----ohos_videocompressor
|     |---- entry  # 示例代码文件夹
|     |---- videoCompressor  # ohos_videocompressor库文件夹
|           |---- index.ets  # 对外接口
|     |---- README_CN.md  # 使用说明文档
```

## 贡献代码

使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/ohos_videocompressor/issues)
给组件，当然，也非常欢迎发 [PR](https://gitee.com/openharmony-sig/ohos_videocompressor/pulls) 共建。

## 开源协议

本项目基于 [Apache License 2.0 ](https://gitee.com/openharmony-sig/ohos_videocompressor/blob/master/LICENSE) ，请自由地享受和参与开源。
